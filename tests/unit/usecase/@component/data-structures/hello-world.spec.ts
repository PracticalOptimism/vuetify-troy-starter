import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import HelloWorld from '@/usecase/@component/data-structures/hello-world.vue'

describe('src/usecase/@component/data-structures/hello-world.vue', () => {
  it('should render the message property to the html template', () => {
    const message = 'new message'
    const wrapper = shallowMount(HelloWorld, {
      propsData: { message }
    })
    expect(wrapper.text()).to.include(message)
  })
})
